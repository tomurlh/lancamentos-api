package servicos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.Lancamento;
import model.Pessoa;
import util.JpaUtil;
import util.TipoLancamento;

public class LancamentoService {

	EntityManager em = JpaUtil.getEntityManager();

	public List<Lancamento> obterTodos() {
		TypedQuery<Lancamento> query = em.createQuery("select p from Lancamento p", Lancamento.class);
		
		return query.getResultList();
	}

	
	
	public Lancamento obter(String id) {
		Lancamento lancamento = em.find(Lancamento.class, Integer.parseInt(id));
		return lancamento;
	}

	
	
	public Lancamento cadastrar(
			String pessoaId, String tipo, 
			String descricao, String valor,
			String dataVencimento, String pago
	) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		if(pessoaId == null || dataVencimento == null)
			return null;
		
		Lancamento lancamento = new Lancamento();
		Pessoa pessoaLancamento = em.find(Pessoa.class, Integer.parseInt(pessoaId));

		em.getTransaction().begin();
		
		if(tipo != null) lancamento.setTipo(tipo.equals("DESPESA") || tipo.equals("Despesa") ? TipoLancamento.DESPESA : TipoLancamento.RECEITA);
		if(tipo != null) lancamento.setPessoa(pessoaLancamento);
		if(tipo != null) lancamento.setDescricao(descricao);
		if(tipo != null) lancamento.setValor(Float.parseFloat(valor));
		if(tipo != null) lancamento.setPago(Boolean.parseBoolean(pago));
		if(tipo != null) lancamento.setDataVencimento(formatter.parse(dataVencimento));
		
		em.persist(lancamento);
		em.getTransaction().commit();

		return lancamento;
	}

	
	
	public Lancamento atualizar(
			String id,String pessoaId, String tipo, 
			String descricao, String valor,
			String dataVencimento, String pago
	) throws ParseException {
		if(id == null || pessoaId == null)
			return null;
		
		Lancamento lancamento = em.find(Lancamento.class, Integer.parseInt(id));
		Pessoa pessoaLancamento = em.find(Pessoa.class, Integer.parseInt(pessoaId));
		
		if (lancamento == null || pessoaLancamento == null)
			return null;
		else {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			em.getTransaction().begin();
			if(tipo != null) lancamento.setTipo(tipo.equals("DESPESA") || tipo.equals("Despesa") ? TipoLancamento.DESPESA : TipoLancamento.RECEITA);
			if(tipo != null) lancamento.setPessoa(pessoaLancamento);
			if(tipo != null) lancamento.setDescricao(descricao);
			if(tipo != null) lancamento.setValor(Float.parseFloat(valor));
			if(tipo != null) lancamento.setPago(Boolean.parseBoolean(pago));
			if(tipo != null) lancamento.setDataVencimento(formatter.parse(dataVencimento));	
			
			em.merge(lancamento);
			em.getTransaction().commit();
			return lancamento;
		}
	}
	
	
	
	public Lancamento remover(String id) {
		Lancamento lancamento = em.find(Lancamento.class, Integer.parseInt(id));
		
		if (lancamento == null)
			return null;
		else {
			em.getTransaction().begin();
			em.remove(lancamento);
			em.getTransaction().commit();
			return lancamento;
		}		
	}
}
