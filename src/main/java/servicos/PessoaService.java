package servicos;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.Pessoa;
import util.JpaUtil;

public class PessoaService {

	EntityManager em = JpaUtil.getEntityManager();

	public List<Pessoa> obterTodos() {
		TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p", Pessoa.class);
		
		return query.getResultList();
	}

	
	
	public Pessoa obter(String id) {
		Pessoa pessoa = em.find(Pessoa.class, Integer.parseInt(id));
		return pessoa;
	}

	
	
	public Pessoa cadastrar(String nome) {
		Pessoa pessoa = new Pessoa(nome);
		em.getTransaction().begin();
		em.persist(pessoa);
		em.getTransaction().commit();

		return pessoa;
	}

	
	
	public Pessoa atualizar(String id, String nome) {
		Pessoa pessoa = em.find(Pessoa.class, Integer.parseInt(id));
		if (pessoa == null)
			return null;
		else {
			em.getTransaction().begin();
			pessoa.setNome(nome);
			em.merge(pessoa);
			em.getTransaction().commit();
			return pessoa;
		}
	}
	
	
	
	public Pessoa remover(String id) {
		Pessoa pessoa = em.find(Pessoa.class, Integer.parseInt(id));
		
		if (pessoa == null)
			return null;
		else {
			em.getTransaction().begin();
			em.remove(pessoa);
			em.getTransaction().commit();
			return pessoa;
		}		
	}
}
