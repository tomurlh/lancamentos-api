package servicos;

import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.post;
import static spark.Spark.put;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.Lancamento;
import model.Pessoa;

public class Main {
	private static PessoaService pessoaService = new PessoaService();
	private static LancamentoService lancamentoService = new LancamentoService();
	private static ObjectMapper om = new ObjectMapper();
	
//	GET /pessoa
//	GET /pessoa/:id
//	POST /pessoa/salvar
//	PUT /pessoa/:id
//	DELETE /pessoa/:id
//
//	GET /lancamento
//	GET /lancamento/:id
//	POST /lancamento/salvar
//	PUT /lancamento/:id
//	DELETE /lancamento/:id
	
	public static void main(String[] args) {
		// Adiciona o cabeçalho CORS na requisição e na resposta, caso não haja 
		options("/*",
        (request, response) -> {

            String accessControlRequestHeaders = request
                    .headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers",
                        accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request
                    .headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods",
                        accessControlRequestMethod);
            }

            return "OK";
        });
		before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
		
		get("/pessoa", (request, response) -> {
			List<Pessoa> pessoas = pessoaService.obterTodos();
			return om.writeValueAsString(pessoas);
		});
		


		get("/pessoa/:id", (request, response) -> {
			Pessoa pessoa = pessoaService.obter(request.params(":id"));
			return om.writeValueAsString(pessoa);
		});
		
		
		
		post("/pessoa/salvar", (request, response) -> {
            String nome = request.queryParams("nome");
            Pessoa pessoaConvertida = om.readValue(request.body(), Pessoa.class);
            Pessoa pessoa = pessoaService.cadastrar(pessoaConvertida.getNome());
            return om.writeValueAsString(pessoa);
		});
		
		
		
		put("/pessoa/:id", (request, response) -> {
            Pessoa pessoa = pessoaService.atualizar(
        		request.params(":id"), request.queryParams("nome")
            );
            return om.writeValueAsString(pessoa);
		});
		
		
		
		delete("/pessoa/:id", (request, response) -> {
			Pessoa pessoa = pessoaService.remover(request.params(":id"));
			return om.writeValueAsString(pessoa);
		});
		
		
		
		
		
		
		get("/lancamento", (request, response) -> {
			List<Lancamento> lancamentos = lancamentoService.obterTodos();
			return om.writeValueAsString(lancamentos);
		});
		


		get("/lancamento/:id", (request, response) -> {
			Lancamento lancamento = lancamentoService.obter(request.params(":id"));
			return om.writeValueAsString(lancamento);
		});
		
		
		
		post("/lancamento/salvar", (request, response) -> {
			Lancamento lancamento = lancamentoService.cadastrar(
	    		request.queryParams("pessoaId"),
	    		request.queryParams("tipo"),
	    		request.queryParams("descricao"),
	    		request.queryParams("valor"),
	    		request.queryParams("dataVencimento"),
	    		request.queryParams("pago")
	    	);
            response.status(201); // 201 Created
            return om.writeValueAsString(lancamento);
		});
		
		
		
		put("/lancamento/:id", (request, response) -> {
			Lancamento lancamento = lancamentoService.atualizar(
        		request.params(":id"),
        		request.queryParams("pessoaId"),
        		request.queryParams("tipo"),
        		request.queryParams("descricao"),
        		request.queryParams("valor"),
        		request.queryParams("dataVencimento"),
        		request.queryParams("pago")
            );
            return om.writeValueAsString(lancamento);
		});
		
		
		
		delete("/lancamento/:id", (request, response) -> {
			Lancamento lancamento = lancamentoService.remover(request.params(":id"));
			return om.writeValueAsString(lancamento);
		});
	}
}
