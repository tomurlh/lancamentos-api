## Lançamentos API

Projeto desenvolvido com [Spark Java Framework](http://sparkjava.com) e [JPA](https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html).
O Spark usado para criação das rotas e JPA como ORM.

O projeto foi desenvolvido para realização de estudo comparativo entre Framework Java (JSF) e JavaScript (React.js), tema de monografia da pós-graduação em *Desenvolvimento de Sistemas com Java*.

### `Rotas`

| End-point  | Resposta | Parâmetros |
| ------------- | ------------- | ------------- |
| **GET /pessoa**  | recupera todas as pessoas  | -  |
| **GET /pessoa/:id**  | recupera uma pessoa específica  | **id** - id da pessoa  |
| **POST /pessoa/salvar**  | cadastra uma pessoa  | **nome** - nome da pessoa no corpo da requisição |
| **PUT /pessoa/:id**  | atualiza uma pessoa  | **id** - no parâmetro da requisição <br> **nome** - no corpo da requisição  |
| **DELETE /pessoa/:id**  | remove uma pessoa  | **id** - id da pessoa  |
| **GET /lancamento**  | recupera todos os lançamentos  | -  |
| **GET /lancamento/:id**  | recupera um lançamento específico  | **id** - id do lançamento  |
| **POST /lancamento/salvar**  | cadastra um lançamento  | **tipo** - tipo da descrição <br> **pessoa_id** - id da pessoa que realizou o lançamento <br> **descricao** - descrição do lançamento <br> **valor** - valor de receita/despesa do lançamento <br> **data_vencimento** - data de vencimento do lançamento <br> **pago** - booleano indicando se lançamento foi pago <br> Todos campos vão no corpo da requisição |
| **PUT /lancamento/:id**  | atualiza um lançamento  | **id** - id do lançamento no parâmetro da requisição <br> No corpo da requisição, todos os campos que compôem um lançamento, menos o **id** |
| **DELETE /lancamento/:id**  | remove um lançamento  | **id** - id do lançamento  |
